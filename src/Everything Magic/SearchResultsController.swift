//  Search_ResultsCollectionViewController.swift
//  Everything Magic
//
//  Created by Spencer McWilliams on 10/5/15.
//  Copyright © 2015 Spencer McWilliams. All rights reserved.
//

import Foundation
import UIKit
import Alamofire

class SearchResultsController: UICollectionViewController {
    
    override func viewDidLoad() {
        self.navigationItem.title = "Results"
    }
    override func viewWillAppear(animated: Bool) {
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        self.view.backgroundColor = UIColor(patternImage: UIImage(named: "background")!)
    }
    
    override func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    override func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return QueryManager.instance.data.count
    }
    
    override func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("card cell", forIndexPath: indexPath) as? CardCell
        let i = QueryManager.instance
        cell?.cardNameLabel.text = QueryManager.instance.data[indexPath.row]["name"].string!
        cell?.cardImageView.image = UIImage(named: "Card Placeholder")
        cell?.networkIndicatorView.startAnimating()
        let cellQ = dispatch_queue_create("cell queue", nil)
        
        dispatch_async(cellQ!, {
            // extract card's image url
            let urlString = QueryManager.instance.data[indexPath.row]["editions"][0]["image_url"].string!
            // request data for card's image
            Alamofire.request(.GET, urlString).responseData { response in
                // update cell when the data is recieved
                let newCell = collectionView.cellForItemAtIndexPath(indexPath) as? CardCell
                newCell?.cardImageView.image = UIImage(data: response.result.value!)
                newCell?.networkIndicatorView.stopAnimating()
            }
        })
        return cell!
    }
}