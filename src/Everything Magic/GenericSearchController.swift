//
//  GenericSearchController.swift
//  Everything Magic
//
//  Created by Spencer McWilliams on 11/19/15.
//  Copyright © 2015 Spencer McWilliams. All rights reserved.
//

import Foundation

class GenericSearchController: SearchFormController {
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(false)
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }
}