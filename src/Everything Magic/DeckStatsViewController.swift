//
//  DeckStatsViewController.swift
//  Everything Magic
//
//  Created by Spencer McWilliams on 11/11/15.
//  Copyright © 2015 Spencer McWilliams. All rights reserved.
//

import Foundation
import UIKit

class DeckStatsViewController: UITableViewController {
    @IBOutlet weak var cmcWebView: UIWebView!
    @IBOutlet weak var colorSpreadWebView: UIWebView!
    @IBOutlet weak var deckNameLabel: UILabel!
    @IBOutlet weak var deckSizeLabel: UILabel!
    
    @IBAction func saveDeckPressed(sender: AnyObject) {
        
    }
    
    enum chartType {
        case BAR
        case PIE
    }
    
    func generateHTMLForGraph(title: String, data: AnyObject..., type: chartType) -> String {
        var selectedType = "broken"
        switch type {
        case .BAR:
            selectedType = "BarChart"
        case .PIE:
            selectedType = "PieChart"
        }
        assert(selectedType != "broken", "divtitle switch broken")

        var dataStringRepresentation = "["
        for var i=0; i<data[0].count!; ++i{
            if i == 0 { dataStringRepresentation.appendContentsOf("['\(data[0][i][0])','\(data[0][i][1])'],") }
            else { dataStringRepresentation.appendContentsOf("['\(data[0][i][0])',\(data[0][i][1])],")}
        }
        dataStringRepresentation.removeAtIndex(dataStringRepresentation.endIndex.predecessor())
        dataStringRepresentation.appendContentsOf("]")
        print(dataStringRepresentation)
        
        return "<html><head><script type=\"text/javascript\" src=\"https://www.google.com/jsapi\"></script><script type=\"text/javascript\">google.load(\"visualization\", \"1\", {packages:[\"corechart\"]});google.setOnLoadCallback(drawChart);function drawChart() {var data = google.visualization.arrayToDataTable(\(dataStringRepresentation));var options = {title: '\(title)', slices: {0:{color:'#B8B0A9'},1:{color:'#9AD3EB'},2:{color:'#83C294'},3:{color:'#E89276'},4:{color:'#FFFCC9',textStyle:{color:'black'}}}, animation: {startup: true, easing: 'out', duration: 1000}};var chart = new google.visualization.\(selectedType)(document.getElementById('chart'));chart.draw(data, options);}</script></head><body><div id='chart' style=\"width: 1400px; height: 1000px;\"></div></body></html>"
    }
    
    override func viewDidLoad() {
        // initialize cmcMultiplier
        DeckManager.instance.clear()
        for card in DeckManager.instance.cards {
            let cardMultiplier = DeckManager.instance.cardMultiplier[DeckManager.instance.cards.indexOf(card)!]
            DeckManager.instance.cmcMultiplier[card["cmc"].intValue] += (DeckManager.instance.cardMultiplier[DeckManager.instance.cards.indexOf(card)!])
            for (var i = 0; i < card["colors"].count; i++) {
                if card["colors"][i].stringValue == "black" { DeckManager.instance.colorSpread[0] += cardMultiplier }
                else if card["colors"][i].stringValue == "blue" { DeckManager.instance.colorSpread[1] += cardMultiplier }
                else if card["colors"][i].stringValue == "green" { DeckManager.instance.colorSpread[2] += cardMultiplier }
                else if card["colors"][i].stringValue == "red" { DeckManager.instance.colorSpread[3] += cardMultiplier }
                else if card["colors"][i].stringValue == "white" { DeckManager.instance.colorSpread[4] += cardMultiplier }
            }
        }
    }
    
    override func viewDidAppear(animated: Bool) {
        // CMC Chart
        let cmcMultiplier = DeckManager.instance.cmcMultiplier
        var data = [["cmc","card count"],["1",cmcMultiplier[1]],["2",cmcMultiplier[2]],["3",cmcMultiplier[3]],["4",cmcMultiplier[4]],["5",cmcMultiplier[5]],["6",cmcMultiplier[6]]]
        self.cmcWebView.loadHTMLString(generateHTMLForGraph("CMC", data: data, type: .BAR) , baseURL: nil)
        
        // Color Spread Chart
        let colorSpread = DeckManager.instance.colorSpread
        data = [["Color Spread", "card count"],["black",colorSpread[0]],["blue",colorSpread[1]],["green",colorSpread[2]],["red",colorSpread[3]],["white",colorSpread[4]]]
        self.colorSpreadWebView.loadHTMLString(generateHTMLForGraph("Color Spread", data: data, type: .PIE) , baseURL: nil)

    }
}

















