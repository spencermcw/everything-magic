//
//  Life_PadViewController.swift
//  Everything Magic
//
//  Created by Spencer McWilliams on 9/18/15.
//  Copyright (c) 2015 Spencer McWilliams. All rights reserved.
//

import Foundation
import UIKit

class Life_PadViewController: UIViewController {
    @IBOutlet weak var player1TableView: UITableView!
    @IBOutlet weak var player2TableView: UITableView!
    @IBOutlet weak var notesTextView: UITextView!
    @IBAction func resetButtonPressed(sender: UINavigationItem) {
        LifeTotals.sharedInstance.resetTotals(LifeTotals.counterType.Pad)
        self.player1TableView.reloadData()
        self.player2TableView.reloadData()
        self.notesTextView.text = LifeTotals.sharedInstance.trackingString
    }
    @IBAction func addLifeCell(sender: UIButton) {
        LifeTotals.sharedInstance.pad[sender.tag].append(0)
        self.player1TableView.reloadData()
        self.player2TableView.reloadData()
    }
    override func viewDidLoad() {
        self.view.backgroundColor = UIColor(patternImage: UIImage(named: "background")!)
        player1TableView.backgroundColor = UIColor.clearColor()
        player2TableView.backgroundColor = UIColor.clearColor()
    }
    
    override func viewWillAppear(animated: Bool) {
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        self.notesTextView.text = LifeTotals.sharedInstance.trackingString
    }
}

extension Life_PadViewController: UITableViewDataSource {
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return LifeTotals.sharedInstance.pad[tableView.tag].count
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("lifeCell\(tableView.tag)") as! Life_UITableViewCell
        cell.backgroundColor = UIColor.clearColor()
        cell.lifeField.text = String(LifeTotals.sharedInstance.pad[tableView.tag][indexPath.row])
        return cell
    }
}

extension Life_PadViewController: UITextFieldDelegate {
    func textFieldDidEndEditing(textField: UITextField) {
        let cell = textField.superview?.superview as! Life_UITableViewCell
        let tmpTableView = cell.superview?.superview as! UITableView
        let index = tmpTableView.indexPathForCell(cell)!.row
        if textField.text != "" {
            LifeTotals.sharedInstance.pad[tmpTableView.tag][index] = Int(textField.text!)!
        }
        else {
            textField.text = String(LifeTotals.sharedInstance.pad[tmpTableView.tag][index])
        }
    }
}

extension Life_PadViewController: UITextViewDelegate {
    func textViewDidEndEditing(textView: UITextView) {
        LifeTotals.sharedInstance.trackingString = textView.text
    }
}