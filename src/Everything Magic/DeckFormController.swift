//
//  DeckFormController.swift
//  Everything Magic
//
//  Created by Spencer McWilliams on 11/9/15.
//  Copyright © 2015 Spencer McWilliams. All rights reserved.
//

import Foundation
import UIKit
import Alamofire

class DeckFormController: UIViewController {
    @IBOutlet weak var nameField: UITextField!
    @IBOutlet weak var cardCollectionView: UICollectionView!
    
    override func viewDidLoad() {
        self.navigationController?.delegate = self
        self.view.backgroundColor = UIColor(patternImage: UIImage(named: "background")!)
        self.cardCollectionView.backgroundColor = UIColor.clearColor()
    }
    
    @IBAction func updateMultiplier(sender: UIStepper) {
        let cell = sender.superview as! DeckCardCell
        let i = self.cardCollectionView.indexPathForCell(cell)?.row
        DeckManager.instance.cardMultiplier[i!] = Int(sender.value)
        cell.multiplierLabel.text = "x" + String(DeckManager.instance.cardMultiplier[i!])        
    }
    
    override func viewWillAppear(animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }
}

extension DeckFormController: UICollectionViewDataSource {
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return DeckManager.instance.cards.count
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("card cell", forIndexPath: indexPath) as! DeckCardCell
        cell.nameLabel.text = DeckManager.instance.cards[indexPath.row]["name"].string!
        cell.multiplierLabel.text = "x" + String(DeckManager.instance.cardMultiplier[indexPath.row])
        
        let cellQ = dispatch_queue_create("cell queue", nil)
        dispatch_async(cellQ!, {
            // extract card's image url
            let urlString = DeckManager.instance.cards[indexPath.row]["editions"][0]["image_url"].string!
            // request data for card's image
            Alamofire.request(.GET, urlString).responseData { response in
                // update cell when the data is recieved
                let newCell = collectionView.cellForItemAtIndexPath(indexPath) as? DeckCardCell
                newCell?.imageView.image = UIImage(data: response.result.value!)
                newCell?.activityIndicator.stopAnimating()
            }
        })
        
        return cell
    }
}

// Used to reload data when coming back to the form from selecting a card.
extension DeckFormController: UINavigationControllerDelegate {
    func navigationController(navigationController: UINavigationController, willShowViewController viewController: UIViewController, animated: Bool) {
        if (viewController == self.navigationController?.viewControllers[0]) {
            self.cardCollectionView.reloadData()
        }
    }
}