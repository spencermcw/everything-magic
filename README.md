# Everything Magic
A swiss army knife for Magic: The Gathering. Written natively for iOS in Swift.

Head to Head
![Head to Head](/screenshots/head-to-head.png "Head to Head")

Incremental Tracking (List-Style)
![List-Style](/screenshots/list.png "List-Style")

Hyperlinked Rules
![Rules](/screenshots/rules.png "Hyperlinked Rules")

Combo Creator
![Combo Creator](/screenshots/combo-creator.png "Combo Creator")

Combo Browser
![Combo Browser](/screenshots/combo-browser.png "Combo Browser")

Deck Creator
![Deck Creator](/screenshots/deck-creator.png "Deck Creator")

Deck Statistics
![Deck Statistics Graphs](/screenshots/deck-statistics.png "Deck Statistics")
