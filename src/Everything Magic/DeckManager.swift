//
//  Deck.swift
//  Everything Magic
//
//  Created by Spencer McWilliams on 10/7/15.
//  Copyright © 2015 Spencer McWilliams. All rights reserved.
//

import Foundation

public class DeckManager {
    public static let instance = DeckManager()
    var cards = [JSON]()
    var cardMultiplier = [Int]()
    var cmcMultiplier = [0,0,0,0,0,0,0,0,0,0]
    // [black,blue,green,red,white]
    var colorSpread = [0,0,0,0,0]
    
    public func clear() -> Void {
        self.cmcMultiplier = [0,0,0,0,0,0,0,0,0,0]
        self.colorSpread = [0,0,0,0,0]
    }
}