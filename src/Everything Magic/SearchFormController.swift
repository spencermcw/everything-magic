//
//  Search_FormViewController.swift
//  Everything Magic
//
//  Created by Spencer McWilliams on 10/2/15.
//  Copyright © 2015 Spencer McWilliams. All rights reserved.
//

import Foundation
import UIKit
import Alamofire

class SearchFormController: UIViewController, NSURLConnectionDataDelegate, UITextFieldDelegate, UIPickerViewDataSource, UIPickerViewDelegate {
    var currentSelector = 0
    var params: [String: String] = [
        "name":     "",
        "cmc":       "",
        "supertype": "",
        "type":      "",
        "subtype":   "",
        "set":       "",
        "oracle":    "",
        "rarity":    "",
        "color":     "",
        "format":    ""
    ]
    
    @IBOutlet weak var superTypeButton: UIButton!
    @IBOutlet weak var subtypeButton:   UIButton!
    @IBOutlet weak var typeButton:      UIButton!
    @IBOutlet weak var setButton:       UIButton!
    
    @IBOutlet weak var cardImageView:   UIImageView!
    
    @IBOutlet weak var pickerViewContainer: UIView!
    @IBOutlet weak var pickerView:          UIPickerView!
    
    @IBAction func search(sender: AnyObject) {
//        var query: String = "https://api.deckbrew.com/mtg/cards?"
//        if (params["name"] != "") { query.appendContentsOf("name=" + params["name"]! + "&") }
//        if (params["cmc"  ] != "") { query.appendContentsOf("cmc="  + params["cmc"]! + "&") }
//        if (params["type"] != "") { query.appendContentsOf("type=" + params["type"]! + "&") }
//        if (params["supertype"] != "") { query.appendContentsOf("supertype=" + params["supertype"]! + "&") }
//        if (params["subtype"] != "") { query.appendContentsOf("subtype=" + params["subtype"]! + "&") }
//        if (params["set"] != "") { query.appendContentsOf("set=" + params["set"]! + "&") }
//        if (params["oracle"] != "") { query.appendContentsOf("oracle=" + params["oracle"]! + "&") }
        
//        NSLog(query, 0)
        Alamofire.request(.GET, "https://api.deckbrew.com/mtg/cards", parameters: params).responseData { response in
            QueryManager.instance.data = JSON.init(data: response.result.value!)
            print(response.request)
            self.performSegueWithIdentifier("resultsSegue", sender: self)
        }
    }
    
    @IBAction func selector(sender: AnyObject) {
        let row = pickerView.selectedRowInComponent(0)
        switch currentSelector {
        case 0:
            params["supertype"] = CardMetaData.supertypes[row]
            superTypeButton.setTitle(params["supertype"], forState: UIControlState.Normal)
        case 1:
            params["type"] = CardMetaData.types[row]
            typeButton.setTitle(params["type"], forState: UIControlState.Normal)
        case 2:
            params["subtype"] = CardMetaData.subtypes[row]
            subtypeButton.setTitle(params["subtype"], forState: UIControlState.Normal)
        case 3:
//            params["set"] = CardMetaData.sets[row]
            setButton.setTitle(params["set"], forState: UIControlState.Normal)
        default:
            NSLog("fucked", 0)
        }
        pickerViewContainer.hidden = true
    }
    
    @IBAction func pickerSelect(sender: AnyObject) {
        currentSelector = sender.tag
        pickerView.reloadAllComponents()
        pickerViewContainer.hidden = false
    }
    
    override func viewDidLoad() {
        pickerViewContainer.hidden = true
        self.view.backgroundColor = UIColor(patternImage: UIImage(named: "background")!)
        self.navigationItem.title = "Search"
    }
    
    override func viewWillAppear(animated: Bool) {
        self.navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }

    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        switch currentSelector {
        case 0: return CardMetaData.supertypes.count
        case 1: return CardMetaData.types.count
        case 2: return CardMetaData.subtypes.count
        case 3: return CardMetaData.sets.count
        default: return 1
        }
    }
    
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        switch currentSelector {
        case 0: return CardMetaData.supertypes[row]
        case 1: return CardMetaData.types[row]
        case 2: return CardMetaData.subtypes[row]
//        case 3: return CardMetaData.sets[row]
        default: return "broken"
        }
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        switch textField.tag {
        case 0: params["name"] = textField.text
        case 1: params["cmc"] = textField.text
        case 2: params["oracle"] = textField.text
        default: return false
        }
        textField.resignFirstResponder()
        return true
    }
}