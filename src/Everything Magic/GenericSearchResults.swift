//
//  SearchResultsViewController.swift
//  Everything Magic
//
//  Created by Spencer McWilliams on 10/25/15.
//  Copyright © 2015 Spencer McWilliams. All rights reserved.
//

import Foundation
import UIKit

class GenericSearchResultsController: SearchResultsController {
    override func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        QueryManager.instance.singleCard = QueryManager.instance.data[indexPath.row]
        performSegueWithIdentifier("showCard", sender: self)
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        QueryManager.instance.clean()
    }
}