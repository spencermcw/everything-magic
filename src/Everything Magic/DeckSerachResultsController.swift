//
//  DeckSerachResultsController.swift
//  Everything Magic
//
//  Created by Spencer McWilliams on 11/9/15.
//  Copyright © 2015 Spencer McWilliams. All rights reserved.
//

import Foundation
import UIKit

class DeckSearchResultsController: SearchResultsController {
    override func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        if !DeckManager.instance.cards.contains(QueryManager.instance.data[indexPath.row]) {
            DeckManager.instance.cards.append(QueryManager.instance.data[indexPath.row])
            DeckManager.instance.cardMultiplier.append(1)
        }
        self.navigationController?.popToViewController((self.navigationController?.viewControllers[0])!, animated: true)
    }
}